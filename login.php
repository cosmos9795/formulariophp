<?php

	session_start();
	if(isset($_POST['usuario']) && isset($_POST['clave'])){
		$usuario = $_POST['usuario'];
		$clave = $_POST['clave'];
		
		for($i=1; $i<=count($_SESSION['admin']);$i++){
			if(($usuario === $_SESSION['admin'][$i]['num_cta']) and ($clave ===$_SESSION['admin'][$i]['clave'] )){
				$aux = $_SESSION['admin'][$i]['nombre'];
				break;
			}
			
		}
		
		if(isset($aux) ){
			$_SESSION['Alumno']=$_SESSION['admin'];
			header('Location: info.php');
		}
		else{
			echo "No registrado";
		}
		
	}

?>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet" href="css/estilo_login.css">
	<link rel="shortcut icon" href="img/logo-login.png" type="image/x-icon">
</head>
<body>
	<div class="contenedor">
	
		<div class="login-form">
		
			<img class="logo-usu" src="img/usuario-logo3.png" alt="imagen-usu">
			<h1 id="box-text">Login</h1>
	
			<form action="validar.php" method="post">
				<label for="cuenta">Número de Cuenta:</label>
				<input type="text" placeholder="Ingresa el usuario" name="usuario">
				<label for="password">Contraseña:</label>
				<input type="password" placeholder="Ingresa la contraseña" name="clave">
				<input class="ent-bot" type="submit" value="Entrar">
				
			</form>
			
		</div>
	</div>
	
</body>
</html>
