<?php
	session_start();
	error_reporting(0);
	if(isset($_SESSION['ident']) and (isset($_SESSION['Alumno']))){
?>

<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Información</title>
	<link rel="stylesheet" href="CSS/estilo_info.css">
</head>
<body>
	<header>
		<div class="barr_fija">
			<div class="barra barra_li">
				<nav>
					<ul>
						<li><a href="info.php">Home</a></li>
						<li><a href="formulario.php">Registrar</a></li>
						<li><a href="cerrar.php">Cerrar sesión</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</header>
	

	<div class="contenedor">
		<div class="usuario">
		<h2>Usuario Autenticado</h2>
			<div class="usu-box">
				<ul>
					<li class="tipo-usu">
						<?php
							print_r($_SESSION['ident']['nombre']);
							print_r(' ');
							print_r($_SESSION['ident']['primer_apellido']);
						?>
					</li>
					<h4>Información :</h4>
					<li>
						<p>Número de cuenta: 
							<?php
								print_r($_SESSION['ident']['num_cta']);
							?>
						</p>
					</li>
					<li>
						<p>Genero: 
							<?php
								print_r($_SESSION['ident']['genero']);
							?>
						</p>
					</li>
					<li>
						<p>Fecha de Nacimiento: 
							<?php
								print_r($_SESSION['ident']['fecha_nac']);
							?>
						</p>
					</li>
				</ul>
				
			
			</div>
		</div>
			
		<div class="datos">
		<h2>Datos Guardados</h2>
			<div class="dat-box">
				<ul class="ul-dat">
					<li>
						#
						<br>
						<?php
							for($i=1;$i<= count($_SESSION['Alumno']);$i++){
								print_r($_SESSION['Alumno'][$i]['num_cta']);
								print("<hr>");
							}
						?>
					</li>
					<li>
						Nombre
						<br>
						<?php
							for($i=1;$i<= count($_SESSION['Alumno']);$i++){
								print_r($_SESSION['Alumno'][$i]['nombre']);
								print_r(" ");
								print_r($_SESSION['Alumno'][$i]['primer_apellido']);
								print("<hr>");
								
							}
						?>
					</li>
					<li>
						Fecha de Nacimiento
						<br>
						<?php
							for($i=1;$i<= count($_SESSION['Alumno']);$i++){
								print_r($_SESSION['Alumno'][$i]['fecha_nac']);
								print("<hr>");
							}
						?>
					</li>

				</ul>
			</div>
		</div>
	</div>

	<footer></footer>
</body>
</html>
<?php 
	}else{
		header('location: login.php');
	}
?>