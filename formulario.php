<?php
	session_start();
	error_reporting(0);
	if(isset($_SESSION['ident']) and (isset($_SESSION['Alumno']))) {
		
		if($_SESSION['ident']['nombre'] != 'Admin'){
				header('location: info.php');
			}
		else{
				
				$clave  = $_POST['cuenta'];
				$nom   = $_POST['nom'];
				$ap_1   = $_POST['apellido_1'];
				$ap_2   = $_POST['apellido_2'];
				$contra = $_POST['password'];
				$gene = $_POST['sexo'];	
				$fech = $_POST['date'];
			if(isset($clave) && isset($nom) && isset($ap_1) && isset($contra) && isset($gene) && isset($fech) ){
				
	
				
				array_push($_SESSION['Alumno'],[
												'num_cta'=>$clave,
												'nombre'=>$nom,
												'primer_apellido' => $ap_1,
												'segundo_apellido'=>$ap_2,
												'clave'=>$contra,
												'genero'=>$gene,
												'fecha_nac' => $fech
											]
						  );
			}
			
				
			
		}


?>
<html>
<head>
    <title>Formulario</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/estilo_form.css">
</head>
    <body>
    	<header>
			<div class="barr_fija">
				<div class="barra barra_li">
					<nav>
						<ul>
							<li><a href="info.php">Home</a></li>
							<li><a href="formulario.php">Registrar</a></li>
							<li><a href="cerrar.php">Cerrar sesión</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		<div class="contenedor">
			
			<form  method="POST">

				<label for="input-text"> <h3>No.Cuenta:</h3>
				<input name="cuenta" type="text" placeholder="Número de cuenta" required>
				</label>

				<label class="" for="input-text"><h3>Nombre:</h3></label>
				<input name="nom" class="form-input " type="text" id="input-nombre" placeholder="Nombre" required>


				<label for="input-text"><h3>Primer Apellido:</h3>
				<input name="apellido_1" type="text" placeholder="Primer Apellido" required>
				</label>

				<label for="input-text"><h3>Segundo Apellido:</h3>
				<input name="apellido_2" type="text" placeholder="Segundo Apellido">
				</label>

				<label  for="input-password"><h3>Contraseña:</h3></label>
				<input name="password" class="form-input" type="password" id="input-password"
						   placeholder="Contraseña" required>

			
				<label for=""><h3>Género:</h3></label>
				<label class="form-radio">
					Hombre<input  type="radio" name="sexo" value="H" checked>
					Mujer<input type="radio" name="sexo" value="M"> 
					Otro<input type="radio" name="sexo" value="O"> 
				</label>

				<label class="form-label" for="input-date"><h3>Fecha:</h3></label>
				<input name="date" class="form-input " type="date" id="input-date"
						   placeholder="fecha" required>
				<button class="bot-env" type="submit">Registrar</button>
			</form>
		</div>

    </body>
</html>
<?php 
		}else{
			header('location: login.php');
		}
?>